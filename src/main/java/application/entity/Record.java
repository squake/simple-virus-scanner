package application.entity;

import lombok.Data;

import java.util.Scanner;

@Data
public class Record {

    private String preface;
    private String name;
    private int offsetStart;
    private int offsetEnd;
    private int length;
    private String hash;
    private String extension;

    private Record(String preface, String hash,int offsetStart, int offsetEnd, int length, String extension, String name) {
        this.preface = preface;
        this.offsetStart = offsetStart;
        this.offsetEnd = offsetEnd;
        this.length = length;
        this.hash = hash;
        this.name = name;
        this.extension = extension;
    }

    @Override
    public String toString() {
        return preface + ":" + name + ":" + offsetStart + ":" +
                offsetEnd + ":" + length + ":" + hash + ":" + extension;
    }

    public static Record getInstance(String record) {
        Scanner scanner = new Scanner(record).useDelimiter(":");

        String preface = scanner.next();
        String hash = scanner.next();
        int start = scanner.nextInt();
        int end = scanner.nextInt();
        int length = scanner.nextInt();
        String ext = scanner.next();
        String name = scanner.next();
        return /*new Record(scanner.next(), scanner.next(),
                scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.next(), scanner.next())*/
        new Record(preface, hash, start, end, length, ext, name);
    }
}
