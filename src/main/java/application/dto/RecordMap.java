package application.dto;

import application.entity.Record;

import java.util.TreeMap;

public class RecordMap {

    private static RecordMap instance;
    private TreeMap<String, Record> records = new TreeMap<>();

    public void addRecord(String preface, Record record) {
        records.put(preface, record);
    }

    public int getTotal() {
        return records.size();
    }

    public static synchronized RecordMap getInstance() {
        if (instance == null) {
            instance = new RecordMap();
        }
        return instance;
    }

    private RecordMap() {
    }

    public TreeMap<String, Record> getRecords() {
        return records;
    }
}
