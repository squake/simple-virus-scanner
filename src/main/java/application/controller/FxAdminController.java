package application.controller;

import application.util.FilePreparer;
import application.util.FileSearcher;
import application.util.NotificationType;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FxAdminController extends FxController {

    protected Parent parent;
    protected Scene scene;
    protected Stage window;

    private boolean isPE = false;
    private FileSearcher fileSearcher;

    @FXML
    private Label filesFound;

    private final DirectoryChooser directoryChooser = new DirectoryChooser();

    @FXML
    private void updateFilesFoundLabel(int total) {
        filesFound.setText(String.valueOf(total));
    }

    public void loadAddSignature(ActionEvent event) throws IOException {
        deselectHyperLink(event);
        loadUI(event, "templates/av_add_signature.fxml");
    }

    public void loadDatabaseView(ActionEvent event) throws IOException {
        deselectHyperLink(event);
        loadUI(event, "templates/av_load_db.fxml");
    }

    public void loadPreparedForScanView(ActionEvent event) throws IOException {
        deselectHyperLink(event);
        loadUI(event, "templates/av_prepared_for_scan.fxml");
    }

    private void loadUI(ActionEvent event) throws IOException {
        parent = FXMLLoader.load(getClass().getClassLoader().getResource("templates/av.fxml"));
        setStageAndScene(event);
    }

    private void loadUI(ActionEvent event, String ui) throws IOException {
        parent = FXMLLoader.load(getClass().getClassLoader().getResource(ui));
        setStageAndScene(event);
    }

    private void setStageAndScene(ActionEvent event) {
        scene = new Scene(parent);
        window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    public void switchSceneToUser(ActionEvent event) throws IOException {
        parent = FXMLLoader.load(getClass().getClassLoader().getResource("templates/uv.fxml"));
        setStageAndScene(event);
    }

    protected void deselectHyperLink(ActionEvent event) {
        Hyperlink hyperlink = (Hyperlink) event.getSource();
        hyperlink.setVisited(false);
    }

    protected void setFocusColor(Control control, boolean malformed) {
        if (control instanceof JFXTextField) {
            JFXTextField textField = (JFXTextField) control;
            if (malformed) {
                textField.setUnFocusColor(Color.rgb(227, 0, 77));
                textField.setFocusColor(Color.rgb(227, 0, 77));
            } else {
                textField.setUnFocusColor(Color.rgb(77, 77, 77));
                textField.setFocusColor(Color.rgb(77, 77, 77));
            }
        }
    }

    public void clear(ActionEvent event) {
        Hyperlink button = (Hyperlink) event.getSource();
        scene = button.getScene();
        JFXTextField dirpathNameField = (JFXTextField) scene.lookup("#dirpathName");
        dirpathNameField.setText(null);
    }

    public void browse(ActionEvent event) {
        deselectHyperLink(event);
        Hyperlink button = (Hyperlink) event.getSource();
        scene = button.getScene();
        JFXTextField dirpathNameField = (JFXTextField) scene.lookup("#dirpathName");
        directoryChooser.setTitle("Select Directory for scan");
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        File dir = directoryChooser.showDialog(window);
        if (dir != null) {
            dirpathNameField.setText(dir.getAbsolutePath());
        } else {
            dirpathNameField.setText(null);
        }
    }

    public void filterPe(ActionEvent event) {
        isPE = !isPE;
    }

    public void prepareFiles(ActionEvent event) {
        JFXButton button = (JFXButton) event.getSource();
        scene = button.getScene();
        ListView<String> preparedFilesList = (ListView<String>) scene.lookup("#preparedFiles");
        JFXTextField dirpathNameField = (JFXTextField) scene.lookup("#dirpathName");

        if (dirpathNameField.getText() == null) {
            notificate("Dirpath field is empty!", NotificationType.ERROR);
            return;
        }

        File root = new File(dirpathNameField.getText());
        /*Timer timer = new Timer("Label Files Found Timer");*/

        if (root.exists()) {
            fileSearcher = FileSearcher.getInstance();

            Thread fileSearcherThread = new Thread(() -> {

                List<File> files = new ArrayList<>(fileSearcher.prepareFiles(root.toPath()));
                List<File> peFiles;

                if (isPE) {
                    peFiles = new ArrayList<>();
                    for (File file : files) {
                        if (FilePreparer.isPE(file))
                            peFiles.add(file);
                    }
                    files.clear();
                    files.addAll(peFiles);
                }


                Platform.runLater(() -> {
                    List<String> filesItems = files.stream().map(File::toString).collect(Collectors.toList());
                    ObservableList<String> items = FXCollections.observableArrayList(filesItems);
                    preparedFilesList.setItems(items);
                    //timer.cancel();
                    updateFilesFoundLabel(fileSearcher.getTotal());
                });
            });

            fileSearcherThread.setName("File Searcher");
            fileSearcherThread.setDaemon(true);
            fileSearcherThread.start();

            /*timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() -> {
                        updateFilesFoundLabel(fileSearcher.getTotal());
                    });
                }
            }, 0, 250);
            updateFilesFoundLabel(fileSearcher.getTotal());*/
        } else {
            notificate("Invalid dirpath!", NotificationType.ERROR);
        }
    }
}