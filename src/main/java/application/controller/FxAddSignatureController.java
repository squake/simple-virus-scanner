package application.controller;

import application.dto.RecordMap;
import application.entity.Record;
import application.util.NotificationType;
import application.util.RecordLoader;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;

public class FxAddSignatureController extends FxAdminController {

    @FXML
    private JFXTextArea body;
    @FXML
    private JFXTextArea preview;
    @FXML
    private JFXTextField name;

    private String encode(ActionEvent event) {
        return DigestUtils.md5Hex(body.getText());
    }

    public void generateSignature(ActionEvent event) throws IOException {
        String encodedBody = encode(event);
        JFXTextField extField = (JFXTextField) scene.lookup("#extension");
        JFXTextField offsetField = (JFXTextField) scene.lookup("#offset");
        JFXTextField prefaceField = (JFXTextField) scene.lookup("#preface");

        if (!offsetField.getText().matches("\\d+-\\d+")) {
            setFocusColor(offsetField, true);
            notificate("Malformed input!", NotificationType.ERROR);
            return;
        } else {
            setFocusColor(offsetField, false);
        }

        String signature = prefaceField.getText()
                + ":" + encodedBody
                + ":" + 10
                + ":" + 20
                + ":" + 30
                + ":" + extField.getText()
                + ":" + name.getText();
        preview.setText(signature);

        notificate("Signature created", NotificationType.SUCCESS);

        Record newRecord = Record.getInstance(signature);
        RecordMap map = RecordMap.getInstance();
        map.addRecord(newRecord.getPreface(), newRecord);
        RecordLoader.unloadRecord();
    }
}
