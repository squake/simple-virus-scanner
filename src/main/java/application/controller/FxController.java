package application.controller;

import application.util.NotificationType;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import org.controlsfx.control.Notifications;

public class FxController {
    protected void notificate(String message, NotificationType type) {
        Notifications notification = Notifications
                .create()
                .title(" ")
                .text(message)
                .graphic(new ImageView(type.getType()))
                .darkStyle()
                .position(Pos.BOTTOM_CENTER);
        notification.show();
    }
}
