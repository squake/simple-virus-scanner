package application.controller;

import application.dto.RecordMap;
import application.entity.Record;
import application.util.RecordLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class FxLoadDbController extends FxAdminController {

    @FXML
    private ListView database;

    public void loadDatabase(ActionEvent event) throws IOException, URISyntaxException {

        RecordMap map = RecordLoader.loadRecords();

        List<String> signatures = new ArrayList<>();
        for (Record s : map.getRecords().values()) {
            signatures.add(s.toString());
        }

        ObservableList<String> items = FXCollections.observableArrayList(signatures);
        database.setItems(items);
    }
}
