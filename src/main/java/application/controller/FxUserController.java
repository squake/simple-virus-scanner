package application.controller;

import application.util.NotificationType;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class FxUserController extends FxController {

    public void scan(ActionEvent event) {
        notificate("Scan successful", NotificationType.SUCCESS);
    }

    public void switchSceneToAdmin(ActionEvent event) throws IOException {
        Parent adminViewParent = FXMLLoader.load(getClass().getClassLoader().getResource("templates/av.fxml"));
        Scene adminViewScene = new Scene(adminViewParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(adminViewScene);
        window.show();
    }

}
