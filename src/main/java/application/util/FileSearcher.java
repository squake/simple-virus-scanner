package application.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class FileSearcher {

    private static FileSearcher instance;
    private FileMonitor monitor;
    private Set<File> foundedFiles;
    private UnzipUtil unzipUtil = new UnzipUtil();

    private AtomicInteger filesCounter = new AtomicInteger(0);

    public Set<File> prepareFiles(Path root) {
        filesCounter.set(0);
        foundedFiles = new HashSet<>();

        try {
            monitor = new FileMonitor(instance, true);
        } catch (IOException e) {
            System.out.println("ERROR : A problem encountered during creating MONITOR.");
        }

        if (monitor != null) {
            monitor.start();

            try {
                Files.walkFileTree(root, new FileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        monitor.register(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    /*
                     *   Здесь был найден файл (он может также быть архивом)
                     *   но сейчас нет разницы - обычный это файл или архив
                     * */
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs){
                        if(unzipUtil.isZip(file.toFile())) {
                                Set<File> unzipFiles = unzipUtil.unzipArchive(file.toFile());
                                foundedFiles.addAll(unzipFiles);
                                filesCounter.addAndGet(unzipFiles.size());
                        }else{
                            foundedFiles.add(file.toFile());
                            filesCounter.incrementAndGet();
                        }
                        return FileVisitResult.CONTINUE;
                    }

                    /*
                     *   Здесь могла случиться проблема с открытием файла.
                     *   Это подойдет в том случае, если архив запаролен
                     *
                     */
                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException exc){
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc){
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                System.out.println("ERROR : A problem encountered during walking through directories.");
            } finally {
                unzipUtil.deleteUnzipFiles();
                monitor.finishMonitoring();
            }
        }
        return foundedFiles;
    }


    public static synchronized FileSearcher getInstance() {
        if (instance == null) {
            instance = new FileSearcher();
        }
        return instance;
    }

    private FileSearcher() {
    }

    public void addFile(File file) {
        foundedFiles.add(file);
        filesCounter.incrementAndGet();
    }

    public void removeFile(File file) {
        foundedFiles.remove(file);
        filesCounter.decrementAndGet();
    }

    public int getTotal() {
        return filesCounter.get();
    }
}
