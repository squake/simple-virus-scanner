package application.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UnzipUtil {

    private static ArrayList<File> unzipDirs = new ArrayList<>();
    static Logger logger = LogManager.getLogger(UnzipUtil.class);

    public Set<File> unzipArchive(File archive) {

        Set<File> unzipFiles = new HashSet<>();

        if (!archive.exists() || !archive.canRead()) {
            logger.error("Archive cannot be unzip!\n");
            return null;
        }

        File unzipDir = new File(archive.getParent(), archive.getName().substring(0, archive.getName().lastIndexOf(".")));
        if (!unzipDir.mkdirs()) {
            logger.error("Directory " + unzipDir + " does not created!\n");
        } else {
            unzipDirs.add(0, unzipDir);
        }

        try (ZipFile zip = new ZipFile(archive)) {
            Enumeration entries = zip.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                File entryUnzipFile = new File(unzipDir.getParent(), entry.getName().replace("/", "\\"));
                if (entry.isDirectory()) {
                    unzipDirs.add(0, entryUnzipFile);
                    new File(archive.getParent(), entry.getName()).mkdirs();
                } else {
                    write(zip.getInputStream(entry),
                            new BufferedOutputStream(new FileOutputStream(entryUnzipFile)));
                    if (!isZip(entryUnzipFile)) {
                        unzipFiles.add(entryUnzipFile);
                    }
                }
                if (isZip(entryUnzipFile)) {
                    unzipFiles.addAll(unzipArchive(entryUnzipFile));
                }
            }
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
        logger.info("Unzip " + unzipDir + "\n");
        return unzipFiles;
    }

    public boolean isZip(File file) {
        if (file == null || !file.exists() && file.isDirectory()) {
            return false;
        }
        String extension;
        if (file.getName().contains(".")) {
            extension = file.getName().substring(file.getName().indexOf("."));
            return extension.equals(".zip") || extension.equals(".zipx") || extension.equals(".gzip");
        }
        return false;
    }

    private void write(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len;
        while ((len = inputStream.read(buffer)) >= 0)
            outputStream.write(buffer, 0, len);
        outputStream.close();
        inputStream.close();
    }

    public void deleteUnzipFiles() {
        unzipDirs.forEach((dir) ->{
            deleteFiles(dir);
            if(dir.delete()){
                logger.info("Directory " + dir + " was deleted /n");
            }
        });
        unzipDirs.clear();
    }

    private void deleteFiles(File dir) {
        File[] files = dir.listFiles();

        // if this dir is not dir OR dir is empty
        if (files == null || files.length == 0) {
            dir.delete();
            return;
        }

        Arrays.stream(files).forEach((file -> {
            if (file.isDirectory() && file.listFiles().length != 0) {
                deleteUnzipFiles();

            } else {
                file.delete();
            }
        }));
    }
}
