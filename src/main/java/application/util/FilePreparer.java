package application.util;

import java.io.*;

public class FilePreparer {

    public static boolean isPE(File file) {

        try (InputStream is = new FileInputStream(file)) {

            // take e_lfanew value on 3C offset that contains PE address
            // extra 4 bytes stand for DWORD data type
            int e_lfanew = Integer.parseInt("3C", 16) + 4;

            byte[] pe = new byte[e_lfanew];

            if (is.read(pe, 0, e_lfanew) < 64)
                return false;

            // take DWORD (4bytes) of e_lfanew value
            StringBuilder sb = new StringBuilder();

            sb.append((char) pe[0]);
            sb.append((char) pe[1]);

            if (!sb.toString().equals("MZ"))
                return false;

            sb.setLength(0);

            // TODO neg arr size ex
            for (int i = e_lfanew - 1; i != e_lfanew - 1 - 4; i--) {
                sb.append(String.format("%02X", pe[i]));
            }

            // convert from HEX to dec offset of PE header
            int peOffset = Integer.parseInt(sb.toString(), 16);

            // bytes left to read in order to get to PE header
            int offsetToPe = peOffset - e_lfanew;

            // read 'em
            byte[] signature = new byte[offsetToPe];
            is.read(signature, 0, offsetToPe);

            // and finally get PE signature
            byte[] pe_header = new byte[2];
            is.read(pe_header, 0, 2);

            sb.setLength(0);

            for (byte p : pe_header) {
                sb.append((char) p);
            }
            if (sb.toString().equals("PE"))
                return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            return false;
        }

        return false;
    }
}
