package application.util;

import application.dto.RecordMap;
import application.entity.Record;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class RecordLoader {

    public static RecordMap loadRecords() throws IOException, URISyntaxException {

        Path path = Paths.get(ClassLoader.getSystemResource("database/database.txt").toURI());
        List<String> contents = Files.readAllLines(path);

        RecordMap records = RecordMap.getInstance();
        for (String content : contents) {
            Record record = Record.getInstance(content);
            records.addRecord(record.getPreface(), record);
        }

        return records;
    }

    public static void unloadRecord() throws IOException {
        Path path = null;
        try {
            path = Paths.get(ClassLoader.getSystemResource("database/database.txt").toURI());

            RecordMap map = RecordMap.getInstance();
            Files.write(path, () -> map.getRecords().entrySet().stream()
                    .<CharSequence>map(e -> e.getValue().toString())
                    .iterator());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
