package application.util;

public enum NotificationType {
    SUCCESS ("images/success.png"),
    ERROR ("images/error.png");

    private String type;

    NotificationType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
