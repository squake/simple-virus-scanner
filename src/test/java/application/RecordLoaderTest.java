package application;

import application.dto.RecordMap;
import application.util.RecordLoader;
import org.junit.Test;

import java.io.IOException;

public class RecordLoaderTest {

    @Test
    public void loadRecords() {
        try {
            RecordMap map = RecordLoader.loadRecords();
            System.out.println(map.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}